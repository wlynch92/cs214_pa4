CFLAGS= -Wall -g

all: search index 

index: libsl index.o index-main.o
	gcc $(CFLAGS) index-main.o index.o -o index -L. -lsl

index-main.o: index-main.c
	gcc $(CFLAGS) -c index-main.c

index.o: index.c index.h libsl
	gcc $(CFLAGS) -c index.c 

libsl: sorted-list.o 
	ar rcs libsl.a sorted-list.o

sorted-list.o: sorted-list.c sorted-list.h
	gcc $(CFLAGS) -c sorted-list.c

search: index.o search.o libsl
	gcc $(CFLAGS) search.o index.o -o search -L. -lsl

search.o: search.c search.h
	gcc $(CFLAGS) -c search.c 

clean:
	rm *.o libsl.a index search
