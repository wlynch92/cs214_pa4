#include "index.h"
/* 
 * Program takes in 2 variables:
 * 1) File to output result to
 * 2) Input file or directory to index
 *
 * If incorrect number of arguements are given, error is thrown
 */


int main(int argc, char* argv[]){
    
    if (argc != 3) {
        fprintf(stderr, "Incorrect arguments given.\n");
        return 1;
    }
    
    Index *indexer = indexerCreate();
    indexStart(argv[2], indexer);
    writeIndex(indexer,argv[1]);
    indexerDestroy(indexer);
    return 0;
}

