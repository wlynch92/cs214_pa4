#ifndef INDEX_H
#define INDEX_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <dirent.h>
#include <ctype.h>
#include "sorted-list.h"

/*
 * index
 * Written by   Bill Lynch  <wlynch92@eden.rutgers.edu>
 *              Jenny Shi   <jenny.shi@rutgers.edu>
 */

struct index {
    struct SortedList *tokenlist;
    int wordcount;
};
typedef struct index Index;

int compareInts(void *p1, void *p2);
int compareNodeCount(const void *p1, const void *p2);
int compareStrings(void *p1, void *p2);

Index* indexerCreate();
Index* indexerFileCreate(char* filename);

SLNode *getFileList(Index *indexer, char *target);

void indexerDestroy(Index *indexer);
void insert(Index *indexer, char* lowercase, char* filename, int freq);

void indexStart(char* input, Index *indexer);
void parseIndexerFile(FILE* file, char* filename, Index *indexer);
void indexFile(FILE* file, char* filename, Index *indexer);
void indexDir(DIR* dirname,char* parent, Index *indexer);
void writeIndex(Index *indexer, char* filename);

#endif
